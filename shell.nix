with import <nixpkgs> {};
let 
  cputils = import ./default.nix;
  pypackages = with python36Packages; [
    cputils

    flake8
    black
    pip
  ]; 
in (python36.buildEnv.override rec {
  extraLibs = pypackages;
}).env
