def connect_mssql(prod=False):
    """Connects to MSSQL

    Args:
        prod: If truthy, connects to the production server, and to the test
              server otherwise.

    Returns:
        A pair. The first element is the connection object, the second an open
        cursor.
    """
    from boto3 import client

    from pymssql import connect

    ssm = client("ssm")

    param = "/mssql/" + ("prod" if prod else "test") + "/"

    server = ssm.get_parameter(Name=param + "host", WithDecryption=True)["Parameter"][
        "Value"
    ]
    user = ssm.get_parameter(Name=param + "username", WithDecryption=True)["Parameter"][
        "Value"
    ]
    password = ssm.get_parameter(Name=param + "password", WithDecryption=True)[
        "Parameter"
    ]["Value"]

    conn = connect(server, user, password, "CelsiusProApp")
    cursor = conn.cursor()

    return (conn, cursor)
