def connect_mongo(prod=False, rw=False):
    """Connects to a MongoDB instance.

    Args:
        prod: If truthy, connects to the production instance. Otherwise the
        test instance
        rw:   If truthy, connects with write capabilities

    Returns:
        A connection object
    """

    from boto3 import client
    from pymongo import MongoClient

    param = (
        "/mongo/"
        + ("prod" if prod else "test")
        + "/"
        + ("rw" if rw else "ro")
        + "/connstr"
    )
    url = client("ssm").get_parameter(Name=param, WithDecryption=True)["Parameter"][
        "Value"
    ]

    return MongoClient(url)
