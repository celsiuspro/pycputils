import pandas as pd
import numpy as np


def loadDly(files, underlyings):
    """
    Reads DLY files from GHCN
    :param files: the list of file names
    :param underlyings: which underlyings to read from the files (list of GHCN abbreviations, like 'PRCP')
    :return: a dataframe
    """
    def load_one_file(file):
        res = None
        d = pd.read_fwf(file,
                        widths=[11, 6, 4] + [5, 1, 1, 1] * 31,
                        header=None,
                        )

        d.rename(columns={0: 'mapping1', 1: 'yearmonth', 2: 'underlying'}, inplace=True)
        d.yearmonth = d.yearmonth.astype(str)
        d = d[d.underlying.isin(underlyings)]

        for i in range(0, 31):
            base = 3 + i * 4
            temp = d.iloc[:, list(range(0, 3)) + list(range(base, base + 4))]
            temp = temp[temp[base] != -9999]
            temp['date'] = pd.to_datetime(temp.yearmonth + f"{(i+1):02}", format="%Y%m%d")
            temp.rename(columns={base: 'value', (base + 1): 'm', (base + 2): 'q', (base + 3): 's'}, inplace=True)
            temp.drop(columns=['yearmonth'], inplace=True)
            if res is None:
                res = temp
            else:
                res = res.append(temp, sort=False)

        return res.sort_values(by=['date'])

    return pd.concat([load_one_file(f) for f in files])


def load_station_list():
    """
    Loads the official station information from GHCN website
    :return: a dataframe
    """
    stations_ghcn = pd.read_fwf('https://www1.ncdc.noaa.gov/pub/data/ghcn/daily/ghcnd-stations.txt', header=None,
                                colspecs=[
                                    (0, 11), (12, 20), (21, 30), (31, 37), (38, 40), (41, 71), (72, 75), (76, 79),
                                    (80, 85)
                                ], names=['mapping1', 'lat', 'lng', 'elev', 'state', 'name', 'gsn', 'hcn', 'wmo'],
                                dtype={'wmo': str})
    stations_ghcn['WMO'] = np.where(stations_ghcn['ghcn'].str[2] == 'M', stations_ghcn['ghcn'].str[-5:], 'nan')
    stations_ghcn['WBAN'] = np.where(stations_ghcn['ghcn'].str[2] == 'W', stations_ghcn['ghcn'].str[-5:], '')
    stations_ghcn['CoCoRaHS'] = np.where(stations_ghcn['ghcn'].str[2] == '1', stations_ghcn['ghcn'].str[-8:], '')
    stations_ghcn['USCoop'] = np.where(stations_ghcn['ghcn'].str[2] == 'C', stations_ghcn['ghcn'].str[-6:], '')
    stations_ghcn['ECA&D'] = np.where(stations_ghcn['ghcn'].str[2] == 'E', stations_ghcn['ghcn'].str[-8:], '')
    stations_ghcn['NMIS'] = np.where(stations_ghcn['ghcn'].str[2] == 'N', stations_ghcn['ghcn'].str[-8:], '')
    stations_ghcn['RAWS'] = np.where(stations_ghcn['ghcn'].str[2] == 'R', stations_ghcn['ghcn'].str[-8:], '')
    stations_ghcn['SNOTEL'] = np.where(stations_ghcn['ghcn'].str[2] == 'S', stations_ghcn['ghcn'].str[-8:], '')
    stations_ghcn['country'] = stations_ghcn['ghcn'].str[0:2]
