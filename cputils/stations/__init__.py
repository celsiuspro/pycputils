import pandas as pd
import numpy as np
from pymongo.errors import BulkWriteError


def __noNan(v):
    return None if v is None or np.isnan(v) else v


def __query_ws(literal, cursor):
    cursor.execute(
        "SELECT WeatherStationID FROM WeatherStations WHERE literalcode = %s", literal
    )
    st = pd.DataFrame(list(cursor), columns=["id"])
    if len(st) == 0:
        return None
    if len(st) > 1:
        print("Literal %s found more than once" % literal)
        return None
    return st.iloc[0, 0]


def __insertSqlRow(row, conn, cursor):
    name = row["name"]
    country = int(row["countryId"])
    wmo = int(__noNan(row["wmo"]) or 0)
    lng = float(row["lon"])
    lat = float(row["lat"])
    alt = float(__noNan(row["alt"]) or 0)
    provid = int(row["provId"])
    mapping1 = str(row["mapping1"])
    mapping2 = str(row["mapping2"]) if __noNan(row["mapping2"]) else None
    literal = str(row["literal"])
    typeid = int(str(row["typeId"]))
    re = int(str(row["re"])) if __noNan(row["re"]) else None
    re2 = int(str(row["re2"])) if __noNan(row["re2"]) else None

    try:
        cursor.execute(
            """INSERT INTO weatherstations
                (WeatherstationID, CountryID, WMO, LiteralCode, Latitude,
                Longitude, Altitude, WeatherStationTypeID, Name,
                WeatherDataProviderID, WeatherRegionID, NewReWeatherRegionId,
                ProviderMapping, ProviderMapping2)
               VALUES
               ((select max(WeatherstationID)+1 from weatherstations), %d, %d,
                %s, %d, %d, %d, %d, %s, %d, %d, %d, %s, %s)""",
            (
                int(country),
                wmo,
                literal,
                float(lat),
                float(lng),
                float(alt),
                typeid,
                name,
                provid,
                re,
                re2,
                mapping1,
                mapping2,
            ),
        )
        cursor.execute(
            """INSERT INTO literals
               (LiteralCode, NameEn, NameDe, NameFr, NameEs, NameNl)
               VALUES (%s, %s, %s, %s, %s, %s)""",
            (literal, name, name, name, name, name),
        )
        conn.commit()
        return True
    except Exception as e:
        conn.rollback()
        raise e


def __insertMongoRow(row, collection, cursor):
    d = {"name": row["name"], "CountryID": int(row["countryId"]), "country": row["countryName"],
         "wmo": int(__noNan(row["wmo"]) or 0), "loc": {"lng": float(row["lon"]), "lat": float(row["lat"])},
         "elevation": float(__noNan(row["alt"]) or 0), "id_provider": int(row["provId"]),
         "mapping1": str(row["mapping1"]), "mapping2": str(row["mapping2"]) if __noNan(row["mapping2"]) else None,
         "data_availability": {
             "msRain": bool(__noNan(row["rain"]) or False),
             "msRainHourly": bool(__noNan(row["rainH"]) or False),
             "msRain08to18": bool(__noNan(row["rain8to18"]) or False),
             "msTempMax": bool(__noNan(row["tmax"]) or False),
             "msTempMin": bool(__noNan(row["tmin"]) or False),
             "msTempAvg": bool(__noNan(row["tavg"]) or False),
             "msSnowCover": bool(__noNan(row["snowc"]) or False),
             "msSnowAutoCover": bool(__noNan(row["snowac"]) or False),
             "msSnowFresh": bool(__noNan(row["snowf"]) or False),
             "msWindMean": bool(__noNan(row["windmean"]) or False),
             "msWindMax": bool(__noNan(row["windmax"]) or False),
             "msSunshine": bool(__noNan(row["sunshine"]) or False),
             "msRadiation": bool(__noNan(row["radiation"]) or False),
             "msHumidity": bool(__noNan(row["humidity"]) or False),
             "msPressure": bool(__noNan(row["pressure"]) or False),
         }}
    literal = row["literal"]

    id_station = __query_ws(literal, cursor)
    if id_station is None:
        print("No weather station %s (literal %s) in SQL!" % (d["name"], literal))
        return False
    d["id_station"] = int(id_station)

    print("Inserting station %s" % d["name"])
    collection.insert_one(d)

    return True


def insert_stations(to_insert, mongo, mssql, cursor):
    """Inserts stations in the system.

    Args:
        to_insert: A Pandas DataFrame containing one row per station, and the
            following columns (! means compulsory, otherwise the column can be
            present but empty, and the default is indicated):
	    name (str!)
	    countryId (int!)
	    countryName (string!)
	    lon, lat (float!)
	    provId (int!)
	    literal (str!)
	    mapping1 (str!)
            typeId (int!)
	    mapping2 (str "")
	    wmo (int 0)
            re, re2 (int None)
	    alt (float 0)
	    rain (bool False)
	    rainH (bool False)
	    rain8to18 (bool False)
	    tmax (bool False)
	    tmin (bool False)
	    tavg (bool False)
	    snowc (bool False)
	    snowac (bool False)
	    snowf (bool False)
	    windmean (bool False)
	    windmax (bool False)
	    sunshine (bool False)
	    radiation (bool False)
	    humidity (bool False)
	    pressure (bool False)
        mongo: A mongo connection object
        mssql: An pymssql connection object
        cursor:A pyssql cursor from the mssql object

    Returns:
        A connection object
    """

    collection = mongo["wx_ws"]["weather_stations"]
    for i, station in to_insert.iterrows():
        __insertSqlRow(station, mssql, cursor)
        __insertMongoRow(station, collection, cursor)


def __create_data_point(row):
    return {
        "id_station": int(row.stationId),
        "Date": row.date,
        "Measure": float(row.value),
        "DataQuality": int(row.quality),
        "ProviderDataQuality": 0 if np.isnan(row.qualityProv) else int(row.qualityProv),
        "Measure2": float(row.measure2) if 'measure2' in row else None,
        "ProviderFlag": row.providerFlag if 'providerFlag' in row else None
    }


def insert_historical_data(data, mongo):
    """
    Inserts historical data for stations in MongoDB.

    Args:
        data: A Pandas DataFrame containing one row per datapoint, and the
            following columns (! means compulsory, otherwise the column can be
            omitted):
          underlying (str!)
	      stationId (int!)
	      date (datetime!)
	      value (float!)
	      quality (int!)
	      qualityProv (int!)
	      measure2 (float None)
	      providerFlag (? None)
        mongo: A mongo connection object
        mssql: An pymssql connection object
        cursor:A pyssql cursor from the mssql object
    """
    for i in data.stationId.unique():
        for u in data.underlying.unique():
            existing_count = mongo['wx_ws'][u].count_documents({"id_station": int(i)})
            if existing_count > 0:
                print(f'Skipped underlying {u} for station {i}: some data already present')
                continue

            print(f"Inserting into {u} for station {i}")
            section = data[(data.underlying == u) & (data.stationId == i)]
            d = [__create_data_point(tpl) for tpl in section.itertuples()]
            try:
                mongo['wx_ws'][u].insert_many(d, ordered=False)
            except BulkWriteError as bwe:
                print(bwe.details)
