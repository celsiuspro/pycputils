import pandas as pd
import numpy as np
from datetime import timedelta
from bs4 import BeautifulSoup
import requests
import zipfile


def load(files, underlying):
    """
    Loads a historical BOM file
    :param files: list of files to load
    :param underlying: which underlying is stored in the files
    :return: a dataframe containing the data
    """

    def load_one(filename):
        print(filename)
        data = pd.read_csv(filename).iloc[:, 1:7]
        data.rename(columns={'Bureau of Meteorology station number': 'mapping1',
                             'Rainfall amount (millimetres)': 'value',
                             'Period over which rainfall was measured (days)': 'period'}, inplace=True)
        data['date'] = pd.to_datetime(data[['Year', 'Month', 'Day']])
        data.dropna(subset=['mapping1', 'value', 'date'], inplace=True)
        data['underlying'] = underlying
        data['qualityProv'] = 4
        data['quality'] = 4
        data['mapping1'] = data['mapping1'].apply(str)
        data.drop(['Year', 'Month', 'Day'], axis=1, inplace=True)
        data['multigroup'] = 0

        multi = data[data['period'] > 1]
        while len(multi) > 0:
            newOnes = []
            row = multi.iloc[0, :]
            i = row.name
            N = int(row['period'])
            value = row['value'] / N
            base_date = row['date']
            first_date = base_date - timedelta(days=N - 1)

            if len(data[(data.date >= first_date) & (data.date < base_date)]) > 0:
                # data falling in the date range
                over1 = (data.date >= first_date) & (data.date <= base_date)
                # data linked to the time range of other points in this time range
                over2 = data.multigroup.isin(data[over1].multigroup[data[over1].multigroup > 0])
                print(f'Problem at {row["date"]} in aggregated measurement for {row["period"]} days')
                data.loc[over1|over2, 'value'] = np.nan
                data.loc[over1|over2, 'multigroup'] = base_date.timestamp()
                data.loc[over1, 'period'] = 1
            else:
                data.drop(i, inplace=True)
                for d in range(0, -N, -1):
                    date = base_date + timedelta(days=d)
                    new_row = row.copy()
                    new_row['date'] = date
                    new_row['value'] = value
                    new_row['period'] = 1
                    new_row['multigroup'] = base_date.timestamp()
                    newOnes.append(new_row)
                data = data.append(pd.DataFrame(newOnes))

            multi = data[data['period'] > 1]

        data.drop(['period'], axis=1, inplace=True)
        data.dropna(inplace=True)
        return data

    return pd.concat([load_one(f) for f in files])


def load_station_list():
    """
    Retrieves the official list of stations from BOM website
    """
    station_stats = pd.read_fwf('ftp://ftp.bom.gov.au/anon2/home/ncc/metadata/lists_by_element/numeric/numAUS_139.txt',
                                skiprows=4,
                                header=None,
                                engine='c',
                                skipfooter=6,
                                colspecs=[(0, 8), (8, 48), (49, 58), (59, 68), (68, 76), (77, 85), (86, 94), (95, 98)],
                                names=['mapping1', 'name', 'lat', 'lng', 'Start', 'End', 'Years', 'Perc'],
                                )
    station_stats.id = station_stats.id.astype('int32', copy=False)
    station_stats.Start = pd.to_datetime(station_stats.Start, format='%b %Y')
    station_stats.End = pd.to_datetime(station_stats.End, format='%b %Y')
    return station_stats


def __fetch_station_info(station):
    id = f'{station:06}'
    url = f'http://www.bom.gov.au/jsp/ncc/cdio/weatherStationDirectory/d?p_display_type=ajaxStnListing&p_nccObsCode=136&p_stnNum={id}&p_radius=5'
    r = requests.get(url)
    if r.status_code != 200:
        raise Exception(f'Error fetching info for station {station}: bad response')

    soup = BeautifulSoup(r.text, 'html.parser')
    cols = soup.table.tbody.tr.find_all('td')
    pstation = int(cols[1].text)
    if pstation != station:
        raise Exception(f'Error fetching info for station {station}: mismatch (got {pstation})')

    year = int(cols[6].text.split()[0])
    completeness = float(cols[9].text)
    token = cols[10].text

    return year, completeness, token


def download_data(station, path):
    """
    Downloads the rain historical data zip file from BOM for the specified station
    :param station: the station id (from BOM)
    :param path: where to download the file
    """
    start, completeness, token = __fetch_station_info(station)
    if start is None:
        return

    url = f'http://www.bom.gov.au/jsp/ncc/cdio/weatherData/av?p_display_type=dailyZippedDataFile&p_stn_num={station:06}&p_nccObsCode=136&p_c={token}&p_startYear={start}'
    r = requests.get(url, stream=True)
    if r.status_code != 200:
        raise Exception(f'Error downloading data for station {station}')

    target = f'{path}/BOM_{station}_{start}_{completeness}.zip'
    with open(target, 'wb') as f:
        f.write(r.content)

    the_zip_file = zipfile.ZipFile(target)
    ret = the_zip_file.testzip()

    if ret is not None:
        raise Exception(f'Bad zip file for station {station}')

    print(f'Donwloaded data for station {station}')
