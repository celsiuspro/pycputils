import pandas as pd


def load(files):
    """
    Loads historical Meteoswiss files
    :param files: list of files to load
    :return: a dataframe containing the data
    """

    ms_under = {'rre150d0': 'msRain', 'tre200dn': 'msTempMin', 'tre200dx': 'msTempMax',
                'hns000d0': 'msSnowFresh', 'hto000d0': 'msSnowCover', 'sre000d0': 'msSunshine',
                'fkl010d1': 'msWindMax', 'htoautd0': 'msSnowAutoCover', 'fkl010d0': 'msWindMean'}

    def load_one(filename):
        ms = pd.read_csv(filename, sep=';', parse_dates=['time'],
                         na_values=['-']).dropna()
        ms.rename(columns={"abbr": "mapping1", "time": "date"}, inplace=True)
        ms.rename(columns=ms_under, inplace=True)
        underlying = ms.columns.values[2]
        ms.rename(columns={underlying: 'value'}, inplace=True)
        ms['underlying'] = underlying
        ms['quality'] = 4
        ms['qualityProv'] = 4
        return ms

    return pd.concat([load_one(f) for f in files])
