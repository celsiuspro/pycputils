from datetime import datetime, timedelta
import pandas as pd


def check(data, threshold=0.95, years=None, data_up_to=None, per_year=True, max_missing_years=3):
    """
    compute data statistics per station. Supports only one underlying

    :param data: a dataframe with columns date, mapping1 and value
    :param threshold: minimim percentage of days with data in the period between min_year and max_year
    :param years: how many years to check. Defaults to 13
    :param data_up_to: defaults to 2 months ago
    :param per_year: if True, every year must respect the threshold. Default is True
    :param max_missing_years: how many years can not respect the threshold
    :return: a tuple containing the yearly statistics and the final evaluation
    """
    if data_up_to is None:
        last_day = datetime.now() - timedelta(days=2 * 30)
    else:
        last_day = data_up_to

    has_recent_data = data[['mapping1', 'date']].groupby('mapping1').max() >= last_day

    yy = last_day.year
    if years is None:
        y = yy - 13
    else:
        y = yy - years

    stats = data[(data.date.dt.year > y) & (data.date <= last_day)] \
        .groupby([data.mapping1, data.date.dt.year])['value'] \
        .count()
    stats.loc[stats.index.get_level_values('date') != last_day.year] = stats.loc[stats.index.get_level_values(
        'date') != last_day.year] / 365
    stats.loc[stats.index.get_level_values('date') == last_day.year] = stats.loc[stats.index.get_level_values(
        'date') == last_day.year] / last_day.timetuple().tm_yday

    if per_year:
        res = (stats > threshold)
        res = res[res].groupby('mapping1').count()
        has_enough_years = res >= (yy - y - max_missing_years)
        both = pd.DataFrame(has_enough_years) \
            .join(has_recent_data, lsuffix='_enough_years', rsuffix='_recent_data')
        return stats, both.all(axis=1)
    else:
        res = stats.groupby('mapping1').mean()
        return stats, res > threshold
