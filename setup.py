from setuptools import setup

setup(
    name="cputils",
    version="0.1",
    description="CelsiusPro utilities",
    url="http://bitbucket.com/celsiuspro/cputils",
    author="Massimo Redaelli",
    author_email="massimo.redaelli@celsiuspro.com",
    license="Proprietary",
    packages=["cputils", "cputils.mongo", "cputils.mssql", "cputils.stations"],
    install_requires=["pymongo", "boto3", "pymssql", "beautifulsoup4", "requests", "pandas"],
    zip_safe=False,
)
