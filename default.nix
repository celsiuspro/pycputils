with import <nixpkgs> {};
let 
  p = python36Packages;
  pymssql = p.buildPythonPackage rec {
    name = "pymssql-${version}";
    version = "2.1.4";
    src = pkgs.fetchFromGitHub {
        owner = "pymssql";
        repo = "pymssql";
        rev = "2.1.4";
        sha256 = "1c9vjfchr8fakbq0vck85h3w05bf1yd4689208a3ghw9kry751bj";
    };
    buildInputs = with self; [ p.cython p.setuptools-git ];
    propagatedBuildInputs = [freetds] ++( with self; [  ]);
    doCheck = false;
  };
in 
  p.buildPythonPackage rec {
    name = "cputils-${version}";
    version = "0.1";
    src = ./.;
    propagatedBuildInputs = [ pymssql ] ++ (with self; [
      p.pymongo
      p.boto3
      p.pandas
      p.beautifulsoup4
      p.requests
    ]);

    doCheck = false;
  }
